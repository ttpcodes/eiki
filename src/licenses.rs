use num_derive::FromPrimitive;
use num_traits::FromPrimitive;
use regex::{escape, RegexSet};
use std::{error, fmt, io};
use std::collections::{HashSet};

#[derive(Debug)]
pub enum Error {
    Conflicting(HashSet<License>),
    Incompatible(HashSet<License>),
    IO(io::Error),
    Nonexistent,
    Regex(regex::Error),
    Unknown,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::Conflicting(ref licenses) => write!(f, "input matches multiple licenses {:?}", licenses),
            Error::Incompatible(ref licenses) => write!(f, "license is incompatible with {:?}", licenses),
            Error::IO(_) => write!(f, "i/o error while processing licenses"),
            Error::Nonexistent => write!(f, "license not found"),
            Error::Regex(_) => write!(f, "error with license parsing regex"),
            Error::Unknown => write!(f, "couldn't recognize license"),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            Error::Conflicting(_) | Error::Incompatible(_) | Error::Nonexistent | Error::Unknown => None,
            Error::IO(ref err) => Some(err),
            Error::Regex(ref err) => Some(err),
        }
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::IO(err)
    }
}

impl From<regex::Error> for Error {
    fn from(err: regex::Error) -> Error {
        Error::Regex(err)
    }
}

#[derive(Clone, Copy, Debug, Eq, FromPrimitive, Hash, PartialEq)]
pub enum License {
    AGPLv3,
    Apache,
    BSD2,
    BSD3,
    GPLv3,
    LGPLv3,
    MIT,
}

impl License {
    pub fn from_text(text: &str) -> Result<License, Error> {
        let licenses = RegexSet::new(&[
            "GNU AFFERO GENERAL PUBLIC LICENSE",
            "Apache License",
            "Redistribution and use in source and binary forms, with or without\nmodification, are permitted provided that the following conditions are met:",
            "Redistribution and use in source and binary forms, with or without\nmodification, are permitted provided that the following conditions are\nmet:",
            "GNU GENERAL PUBLIC LICENSE",
            "GNU LESSER GENERAL PUBLIC LICENSE",
            &escape("Permission is hereby granted, free of charge, to any person obtaining a copy of \
                this software and associated documentation files (the \"Software\"), to deal in \
                the Software without restriction, including without limitation the rights to \
                use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies \
                of the Software, and to permit persons to whom the Software is furnished to do \
                so, subject to the following conditions:").replace(" ", r"(?: |\n)").replace("\"", "(?:\"|')")
        ])?;
        let matches = licenses.matches(&text);
        // RegexSet.len() doesn't actually return the number of matches: it returns the length of
        // the RegexSet! ain't that wacky?!
        // i'm losing my mind, please send help
        match matches.iter().count() {
            0 => Err(Error::Unknown),
            // why.
            1 => Ok(License::from_usize(matches.iter().collect::<Vec<usize>>()[0]).ok_or(Error::Unknown)?),
            _ => Err(Error::Conflicting(matches.iter().map(|i| License::from_usize(i).ok_or(Error::Unknown)).collect::<Result<HashSet<_>, _>>()?)),
        }
    }

    fn compatible_with(&self) -> HashSet<License> {
        match *self {
            License::AGPLv3 | License::GPLv3 => {
                let mut licenses = HashSet::from([License::AGPLv3, License::GPLv3]);
                licenses.extend(License::LGPLv3.compatible_with());
                return licenses
            },
            License::Apache => {
                let mut licenses = HashSet::from([*self]);
                licenses.extend(License::BSD3.compatible_with());
                return licenses
            },
            License::BSD2 | License::MIT => HashSet::from([License::BSD2, License::MIT]),
            License::BSD3 => {
                let mut licenses = HashSet::from([*self]);
                licenses.extend(License::BSD2.compatible_with());
                return licenses
            },
            License::LGPLv3 => {
                let mut licenses = HashSet::from([*self]);
                licenses.extend(License::Apache.compatible_with());
                return licenses
            }
        }
    }

    pub fn is_compatible_or_err(&self, licenses: &HashSet<License>) -> Result<(), Error> {
        let incompatible: HashSet<License> = licenses.difference(&self.compatible_with()).cloned().collect();
        if incompatible.len() == 0 {
            return Ok(());
        }
        Err(Error::Incompatible(incompatible))
    }
}

use crate::deps::Dep;
use crate::licenses::{Error, License};
use nom::IResult;
use nom::bytes::complete::{tag,take_till};
use nom::character::complete::{newline,tab};
use nom::combinator::{all_consuming, opt};
use nom::multi::fold_many0;
use nom_regex::str::re_find;
use regex::Regex;
use std::fs::read_to_string;
use std::path::Path;

struct Replacement<'a> {
    old:     &'a str,
    new:     &'a str,
    version: &'a str,
}

pub fn get_license(dir: &Path) -> Result<License, Error> {
    for entry in dir.read_dir()? {
        let subpath = entry?.path();
        // let chains aren't a thing yet, so we have this disgusting mess instead
        if !subpath.is_dir() {
            if let Some(file_name) = subpath.file_name() {
                if let Some(string) = file_name.to_str() {
                    if Regex::new(r"^(?i:license(?:\.[a-z]{1,3})?|copying(?:\.lesser)?)$")?.is_match(string) {
                        return License::from_text(&read_to_string(&subpath)?)
                    }
                }
            }
        }
    }
    Err(Error::Nonexistent)
}

pub fn parse_config(input: &str) -> IResult<&str, Vec<Dep>> {
    let (input, _) = tag("module ")(input)?;
    let (input, _) = re_find(Regex::new(r"(?:[a-z0-9\.-]+/)*[a-z0-9\.-]+").unwrap())(input)?;
    let (input, _) = tag("\n\ngo ")(input)?;
    let (input, _) = re_find(Regex::new(r"\d\.\d\d").unwrap())(input)?;
    let (input, _) = tag("\n\nrequire (\n")(input)?;
    let (input, deps) = take_till(|c| c == ')')(input)?;
    let (_, mut deps) = all_consuming(fold_many0(parse_dep, Vec::new, |mut acc: Vec<_>, item| {
        acc.push(item);
        acc
    }))(deps)?;
    let (input, _) = tag(")\n")(input)?;
    let (input, _) = opt(newline)(input)?;
    let (remainder, replacements) = all_consuming(fold_many0(parse_replacement, Vec::new, |mut acc: Vec<_>, item| {
        acc.push(item);
        acc
    }))(input)?;
    for replacement in replacements {
        deps.remove(deps.iter().position(|item| {
            let Dep::Go(name, _) = item;
            // what the actual hell happened here
            *name == replacement.old
        }).expect("dep not found"));
        deps.push(Dep::Go(replacement.new.to_string(), replacement.version.to_string()));
    }

    Ok((remainder, deps))
}

fn parse_dep(input: &str) -> IResult<&str, Dep> {
    let (input, _) = tab(input)?;
    let (input, name) = re_find(Regex::new(r"(?:[a-z0-9\.-]+/)*[a-z0-9\.-]+").unwrap())(input)?;
    let (input, _) = tag(" ")(input)?;
    let (input, version) = re_find(Regex::new(r"v\d+\.\d+\.\d+(?:-\d{14}-[a-f0-9]{12})?").unwrap())(input)?;
    let (input, _) = opt(tag(" "))(input)?;
    let (input, _) = opt(tag("// indirect"))(input)?;
    let (remainder, _) = newline(input)?;

    Ok((remainder, Dep::Go(name.to_string(), version.to_string())))
}

fn parse_replacement(input: &str) -> IResult<&str, Replacement> {
    let (input, _) = tag("replace ")(input)?;
    let (input, old) = re_find(Regex::new(r"(?:[a-z0-9\.-]+/)*[a-z0-9\.-]+").unwrap())(input)?;
    let (input, _) = tag(" => ")(input)?;
    let (input, new) = re_find(Regex::new(r"(?:[a-z0-9\.-]+/)*[a-z0-9\.-]+").unwrap())(input)?;
    let (input, version) = re_find(Regex::new(r"v\d+\.\d+\.\d+(?:-\d{14}-[a-f0-9]{12})?").unwrap())(input)?;
    let (remainder, _) = newline(input)?;

    Ok((remainder, Replacement { old, new, version }))
}

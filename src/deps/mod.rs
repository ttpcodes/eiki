mod go;

use crate::licenses;
use dirs::home_dir;
use std::{error, fmt, io};
use std::fs::read_to_string;
use std::path::Path;

#[derive(Debug)]
pub enum Error {
    IO(io::Error),
    Nom(nom::Err<nom::error::Error<String>>),
    Unknown,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::IO(_) => write!(f, "i/o error while parsing project config"),
            Error::Nom(_) => write!(f, "error using custom nom parser to parse project config"),
            Error::Unknown => write!(f, "no parser found for project"),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self  {
            Error::IO(ref err) => Some(err),
            Error::Nom(ref err) => Some(err),
            Error::Unknown => None,
        }
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::IO(err)
    }
}

impl From<nom::Err<nom::error::Error<String>>> for Error {
    fn from(err: nom::Err<nom::error::Error<String>>) -> Error {
        Error::Nom(err)
    }
}

pub enum Dep {
    Go(String, String),
}

impl Dep {
    pub fn get_license(&self) -> Result<licenses::License, licenses::Error> {
        match self {
            Dep::Go(name, version) => {
                let mut path = home_dir().ok_or(io::Error::new(io::ErrorKind::NotFound, "couldn't find home directory"))?;
                path.push("go");
                path.push("pkg");
                path.push("mod");
                path.push(&format!("{}@{}", name, version));

                go::get_license(&path)
            }
        }
    }
}

pub enum Deps<'a> {
    Go(&'a Path),
}

impl<'a> Deps<'a> {
    pub fn find(dir: &Path) -> Result<Deps, Error> {
        if dir.join("go.mod").is_file() {
            return Ok(Deps::Go(dir));
        }
        Err(Error::Unknown)
    }

    pub fn get(&self) -> Result<Vec<Dep>, Error> {
        match self {
            Deps::Go(dir) => {
                let (_, deps) = go::parse_config(&read_to_string(dir.join("go.mod"))?).map_err(|err| err.map(|err| nom::error::Error::new(err.input.to_string(), err.code)))?;
                Ok(deps)
            }
        }
    }

    pub fn get_license(&self) -> Result<licenses::License, licenses::Error> {
        match self {
            Deps::Go(dir) => go::get_license(dir),
        }
    }
}

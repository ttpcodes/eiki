mod deps;
mod licenses;

use anyhow::{Context, Result};
use std::collections::HashSet;
use std::env::current_dir;

const ERROR_MESSAGE: &str = "error checking license compatibility";

fn main() -> Result<()> {
    // what the bloody hell is this temporary variable unwrap nonsense.
    let pwd = current_dir().with_context(|| ERROR_MESSAGE)?;
    let config = deps::Deps::find(&pwd.as_path()).with_context(|| ERROR_MESSAGE)?;
    config.get_license().with_context(|| ERROR_MESSAGE)?
        .is_compatible_or_err(&config.get().with_context(|| ERROR_MESSAGE)?.iter().map(|dep| dep.get_license()).collect::<Result<HashSet<licenses::License>, licenses::Error>>().with_context(|| ERROR_MESSAGE)?)
        .with_context(|| ERROR_MESSAGE)?;
    Ok(())
}
